import json
import pickle
import pyspark.sql.functions as F 
from BacktestConfig import CacheConfig, AggTradesConfig
from glob     import glob
from datetime import datetime
from pandas   import DataFrame
from pyspark           import SparkContext
from pyspark.sql       import SQLContext
from pyspark.sql.types import StringType

### OFFLINE PART
#   .mat -> JSON             [populateTAQCache] --> Collection
#   JSON -> (clean) parquet  [AggregateTrades, AggregateQuotes] --> Processing
#   Correlation              [Reads from parquet]
#   ImpactModel              [Impact Model]
#   Cointegrateion           [Cointegration]
#   TOBModel                 [TOB Model]

def _addDay(rdd):
    dayMap = F.udf(lambda x: datetime.strftime(datetime.fromtimestamp(x), '%Y%m%d'), StringType())
    tmp = rdd.withColumn('Day', dayMap(rdd.timestamp))
    return tmp

class AggregateTrades:
    def __init__(self, cache, tickers):
        self.pklCache    = cache + '/pkl/trades'      
        self.prqCache    = cache + '/parquet/trades'
        self.trdSummaryCache = cache + '/parquet/summary'
        self.tickers  = tickers  
              
    def run(self):			
        sc = SparkContext(appName="BacktestApp")
        sqlContext = SQLContext(sc)
       
        for t in self.tickers:
            path          = '%s/%s*' % (self.pklCache, t)    
            files         = glob(path)
            idx           = len(files[0].split('/')) - 1
            rawFrames     = [(f.split('/')[idx].rstrip('_trades.pkl'), DataFrame(json.loads(pickle.load(open(f,'rb'))))) for f in files]
            rddTradesTmp  = [(name, sqlContext.createDataFrame(f)) for (name, f) in rawFrames]        
            rddTrades     = [(name, _addDay(rdd)) for (name, rdd) in rddTradesTmp ]
            
            # Save collapsed individual days	
            [rdd.save("%s/%s.parquet" % (self.prqCache, tableName), "parquet", "overwrite") for (tableName,rdd) in rddTrades]

            # Create a summary over days
            [rdd.registerTempTable(tableName) for (tableName,rdd) in rddTrades]
            sqlstmt = (''.join( ['SELECT * FROM %s UNION ' % tableName for (tableName,rdd) in rddTrades]).rstrip('UNION '))
            summaryRDD = sqlContext.sql(sqlstmt)
            summaryRDD.save("%s/%s_Summary.parquet" % (self.trdSummaryCache, t), "parquet", "overwrite")
                        
            pand = summaryRDD.toPandas()
            _grp  = pand.groupby('Day')
            _agg = _grp.agg({'Day': lambda x: x.tail(1),
                             'tradeSize': lambda x: x.sum(),
                             'tradePrice': lambda x: x.tail(1),  
                             'timestamp':  lambda x: x.tail(1)})
            sparkDF = sqlContext.createDataFrame(_agg)            
            sparkDF.save("%s/daily/%s_Daily.parquet" % (self.trdSummaryCache, t), "parquet", "overwrite")
           
            #summaryRDD  = summaryRDD.orderBy('timestamp')            
            #tmp  = summaryRDD.groupBy('Day')
            #tmp  = tmp.agg(F.last('Day').alias('Day'),
            #           F.sum('tradeSize').alias('sumTradeSize'),
            #           F.last('tradePrice').alias('tradePrice'),
            #           F.last('timestamp').alias('timestamp'))
            
            
if __name__ == '__main__':
    AggregateTrades(CacheConfig.baseCache, AggTradesConfig.tickers).run()


