import json
import pickle
from datetime import datetime
from glob        import glob
from pandas      import DataFrame
from pyspark     import SparkContext
from pyspark.sql import SQLContext
from pyspark.sql.functions import first, udf
from pyspark.sql.types     import StringType
from BacktestConfig import CacheConfig, AggQuotesConfig


def _printTbl(tmp, sqlContext):
	tmp.registerTempTable('tmpTable')
	a = sqlContext.sql('SELECT * FROM tmpTable')
	a.show()

class AggregateQuotes:
    def __init__(self, ticker, prqCache, pklCache, sqlcontext):
        self.ticker = ticker
        self.pklCache  = pklCache
        self.prqCache  = prqCache
        self.sqlContext = sqlcontext

    def collapse(self, name, rdd, sqlContext):
		dayMap = udf(lambda x: datetime.strftime(datetime.fromtimestamp(x), '%Y%m%d'))	
		bktMap = udf(lambda x: int((x*60.0)/12.0))	
	
		tmp = rdd.withColumn('intraDayBucket', bktMap(rdd.minutesFromMid)) # Trying to do this inplace will get you into trouble
		tmp = tmp.groupBy('intraDayBucket')	
		tmp = tmp.agg(  first('datetime').alias('datetime'),      first('timestamp').alias('timestamp'), 
				first('midPrice').alias('midprice'),      first('minutesFromMid').alias('minutesFromMid'), 
				first('quoteBidPrice').alias('bidPrice'), first('quoteBidSize').alias('bidSize'),
				first('quoteAskPrice').alias('askPrice'), first('quoteAskSize').alias('askSize'), 
				first('intraDayBucket').alias('intraDayBucket'))
		tmp = tmp.withColumn('Day', dayMap(tmp.timestamp))
		tmp = tmp.withColumn('BookPressure', tmp.bidSize - tmp.askSize)		
		tmpPandas = tmp.toPandas()		
		tmpPandas = tmpPandas.sort('intraDayBucket')					
		tmpPandas['return'] = (tmpPandas['midprice'] - tmpPandas['midprice'].shift(1)).div(tmpPandas['midprice'].shift(1)) 	
		tmpPandas['Fwdreturn'] = tmpPandas['return'].shift(-1)	
		tmpPandas = tmpPandas.dropna()		
		tmp = sqlContext.createDataFrame(tmpPandas)
		return (name, tmp)

   
    def run(self):
        sqlContext = self.sqlContext
        path        = '%s/quotes/%s*' % (self.pklCache, self.ticker)
        #print path   
        files      = glob(path)
        idx        = len(files[0].split('/')) - 1
        rawFrames  = [(f.split('/')[idx].rstrip('_quotes.pkl'), DataFrame(json.loads(pickle.load(open(f,'rb'))))) for f in files]
        rddQuotes  = [(name, sqlContext.createDataFrame(f)) for (name, f)   in rawFrames]        
        rddquotes  = [self.collapse(name, rdd, sqlContext)  for (name, rdd) in rddQuotes]		

        # Save collapsed individual days	
        [rdd.save("%s/quotes/%s.parquet" % (self.prqCache, tableName), "parquet", "overwrite") for (tableName,rdd) in rddquotes]

        # Create a summary over days
        [rdd.registerTempTable(tableName) for (tableName,rdd) in rddquotes]
        sqlstmt = (''.join( ['SELECT * FROM %s UNION ' % tableName for (tableName,rdd) in rddquotes]).rstrip('UNION '))
        summaryRDD = sqlContext.sql(sqlstmt)
        summaryRDD.save( "%s/quotes/%s_Summary.parquet" % (self.prqCache, self.ticker), "parquet", "overwrite")
		
if __name__ == '__main__':
    sc = SparkContext(appName="BacktestApp")
    sqlContext = SQLContext(sc)                
    [AggregateQuotes(t, CacheConfig.prqCache, CacheConfig.pklCache, sqlContext).run() for t in AggQuotesConfig.tickers]

