from BacktestConfig import TOBConfig, CacheConfig
import numpy as np
import statsmodels.api as sm
from pyspark     import SparkContext
from pyspark.sql import SQLContext
from datetime import datetime
from pandas import DataFrame

class EstimateTOBModel:
    def __init__(self, ticker, cache, days, sqlContext):
        self.ticker = ticker
        self.cache   = cache
        self.days    = days
        self.sqlContext = sqlContext
        self.data = self.sqlContext.load("%s/quotes/%s_Summary.parquet" % (self.cache, self.ticker))
        
    def run(self): 
        d = {}      
        d['results'] = {}        
    
        endog = self.data.select('Fwdreturn').toPandas().values        
        exog  = self.data.select('BookPressure').toPandas().values
        mod = sm.OLS(endog, exog)
        res = mod.fit()
       
        d['results']['Coefficient'] = res.params[0]
        sparkTOBModel = self.sqlContext.createDataFrame(DataFrame.from_dict(d))
        sparkTOBModel.save("%s/models/TOB/%s.parquet" % (self.cache,self.ticker), "parquet", "overwrite")
       
if __name__== '__main__':
    sc = SparkContext(appName="BacktestApp")
    sqlContext = SQLContext(sc)       
    days    = TOBConfig.days    
    tickers = TOBConfig.tickers
    cache   = CacheConfig.prqCache
    [EstimateTOBModel(t, cache, days, sqlContext).run()for t in tickers]
