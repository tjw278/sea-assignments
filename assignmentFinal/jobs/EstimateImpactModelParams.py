from pandas      import DataFrame
from pyspark     import SparkContext
from pyspark.sql import SQLContext
from BacktestConfig import CacheConfig, ImpModelConfig

def _addEntrySingleIdx(d,k1,v):
    d[k1] = v

def _addEntryDoubleIdx(d,k1,k2,v):
    d[k1][k2] = v
       
class EstimateImpactModelParams():
    def __init__(self, tickers, cache):
        self.tickers = tickers
        self.cache   = cache
    
        sc = SparkContext(appName="BacktestApp")
        self.sqlContext = SQLContext(sc)    
                 
        tbls = [(t,self.sqlContext.load("%s/summary/daily/%s_Daily.parquet" % (self.cache,t))) for t in self.tickers ] 
        [tbl.registerTempTable(name) for (name,tbl) in tbls]

        stmt  = "SELECT %s.Day, " % self.tickers[0]
        stmt += ' '.join(['%s.tradePrice as %sPrice , %s.tradeSize as %sVolume ,' % (t,t,t,t) for t in self.tickers]).rstrip(',')
        stmt += 'FROM %s ' % self.tickers[0]
        stmt += ' '.join('LEFT JOIN %s ON %s.Day=%s.Day' % (t,self.tickers[0],t) for t in self.tickers if not (t ==self.tickers[0])) 
        
        self.data = self.sqlContext.sql(stmt)
 
    def run(self):
        d = {}
        data = self.data.toPandas()
        [_addEntrySingleIdx(d,t,{})                                      for t in self.tickers]                
        [_addEntryDoubleIdx(d,t,'ADV', data['%sVolume' % t].mean())      for t in self.tickers]
        [_addEntryDoubleIdx(d,t,'Volatility', data['%sPrice' % t].std()) for t in self.tickers]
        
        sparkImpactModel = self.sqlContext.createDataFrame(DataFrame(d))
        sparkImpactModel.save("%s/models/ImpactModel.parquet" % self.cache, "parquet", "overwrite")
        
if __name__== '__main__': 
    EstimateImpactModelParams(ImpModelConfig.tickers, CacheConfig.prqCache).run()
