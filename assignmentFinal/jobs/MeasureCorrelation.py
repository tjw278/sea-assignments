from pyspark     import SparkContext
from pyspark.sql import SQLContext
from pyspark.sql.functions import last
from BacktestConfig import CorrelConfig, CacheConfig

class MeasureCorrelation:
    def __init__(self, tickers, cache):
        self.tickers = tickers
        self.cache   = cache
    
    def run(self):
        sc = SparkContext(appName="BacktestApp")
        sqlContext = SQLContext(sc)    
                 
        tbls = [(t,sqlContext.load("%s/summary/daily/%s_Daily.parquet" % (self.cache,t))) for t in self.tickers ] 
        [tbl.registerTempTable(name) for (name,tbl) in tbls]

        stmt  = "SELECT %s.Day, " % self.tickers[0]
        stmt += ' '.join(['%s.tradePrice as %s ,' % (t,t) for t in self.tickers]).rstrip(',')
        stmt += 'FROM %s ' % self.tickers[0]
        stmt += ' '.join('LEFT JOIN %s ON %s.Day=%s.Day' % (t,self.tickers[0],t) for t in self.tickers if not (t ==self.tickers[0])) 
        
        df = sqlContext.sql(stmt)        
        pandDf = df.toPandas()
                
        sparkCov = sqlContext.createDataFrame(pandDf.cov())
        sparkCov.save("%s/models/DailyCovariance.parquet" % self.cache, "parquet", "overwrite")
                 
if __name__ == '__main__':
    # MAKE SURE THE TICKER LIST IS SORTED!    
    MeasureCorrelation(CorrelConfig.tickers, CacheConfig.prqCache).run()
