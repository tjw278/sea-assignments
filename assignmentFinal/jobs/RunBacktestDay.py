import pickle
import time
import math
import numpy as np
import logging
from glob           import glob
from pandas         import DataFrame, Series, isnull
from scipy.optimize import fmin_slsqp
from pyspark        import SparkContext, StorageLevel
from pyspark.sql    import SQLContext
from BacktestConfig import BacktestConfig, CacheConfig

def _getPrices(s,ticker,rdd,bucket):
    df = rdd.filter('intraDayBucket=%d' % bucket)
    if not df.count() == 0:    
        s[ticker] = rdd.filter('intraDayBucket=%d' % bucket).toPandas()['midprice']
    else:
        s[ticker] = np.nan
        
def _getBookPressure(s,t,rdd,bucket):
    try:
        s[t] = rdd.filter('intraDayBucket=%d' % bucket).toPandas()['BookPressure']
    except Exception:
        s[t] = np.nan    
        
    #tomLog.debug("Book pressure:")
    #tomLog.debug(s.values)
            
def _getForecast(forecast, tobRatio, ticker, tobCoefficient):
    forecast[ticker] = tobRatio[ticker] * tobCoefficient[ticker] 
    #tomLog.debug("Forecast:")
    #tomLog.debug(forecast.values)

# Shares and amount in volume?
def _impactCostFx(X,ADV,VOL):
    return .142 * VOL * math.sqrt(abs(X) / (ADV*(1.0/(24.0*60.0*12.0)))) 

def _getImpactCost(x, impactMod):
    cost = 0.0 
    for i in range(len(x) -1) :
        y     =  x[i] # these are shares        
        adv   =  impactMod.ix['ADV',i]
        vol   =  impactMod.ix['VOL',i]
        cost += _impactCostFx(y,adv,vol)
    return cost
    
def _eqCons(x, positions, forecast, prices, impactMod, covar, lam, gam, wealth):
    return (-1 + sum(x) + sum(positions))

def weightsToShares(wealth, weights, prices):
      return np.divide(np.multiply(weights, wealth), prices)

  
def objFunction(x, weights, forecast, prices, impactMod, covar, lam, gam, wealth):
    newWeights     = x + weights
    returnComponent = forecast.dot( newWeights )     
    
    shares = weightsToShares(wealth, weights, prices)
    costComponent   = _getImpactCost(shares, impactMod) 
    
    covPartial       = covar.dot(newWeights)
    covPartial.index = weights.index
    varComponent     = newWeights.dot(covPartial)  
    
    result =  -1 * returnComponent + lam*varComponent + gam*costComponent
    #tomLog.debug("Trades")
    #tomLog.debug(x)    
    #tomLog.debug('RETURN %0.9f'  % (returnComponent)) # Max this or min * -1     
    #tomLog.debug('VAR  %0.9f'    % np.multiply(lam,varComponent)) 
    #tomLog.debug('COST %0.9f'    % np.multiply(gam,costComponent))
    #tomLog.debug('Result: %0.9f' % result)
    
    #tomLog.debug("OLD Positions:")
    #tomLog.debug(positions.values)    
    #tomLog.debug("New Positions")
    #tomLog.debug(newPosition.values)    
    return result 
    

class RunBacktestDay():
    def __init__(self, day, cache, sqc, tickers, theta):
        self.day        = day
        self.cache      = cache
        self.sqlContext = sqc
        self.theta      = theta
         
        # Create the market rdd and keep it in memory
        fls  = ['%s/quotes/%s_%s.parquet' % (cache, t, day) for t in tickers]              
        rdds = [self.sqlContext.load(f) for f in fls]        
        [r.persist(StorageLevel.MEMORY_ONLY) for r in rdds]     

        # Create some market state        
        self.tickers   = sorted(tickers)                
        self.weights   = Series(([1.0/len(tickers)] * len(tickers)), index=sorted(tickers))        
        self.market    = dict(zip(tickers,rdds))
       

        # Load the Impact Model
        imp = sqc.load('%s/models/ImpactModel.parquet' % cache)
        imp = imp.toPandas()
        imp.index = ['ADV','VOL']        
        self.impactMod = imp       

        # Load the covariance matrix
        cov = sqc.load('%s/models/DailyCovariance.parquet' % cache)  
        cov = cov.toPandas()
        self.covMat = cov        
      
        # Load the TOB MODELS
        tobModels = Series(index=tickers) 
        for t in tickers:
            mod = self.sqlContext.load('%s/models/TOB/%s.parquet' % (cache,t))
            tobModels[t] = mod.first().results
        tobModels = tobModels.reindex(sorted(tobModels.index))    
        self.tobModels = tobModels             
        
     
    def run(self):
        #startPd =  10.0 * 60.0 * 60.0 / 12.0 
        startPd = 4450
        endPd   =  15.0 * 60.0 * 60.0 / 12.0 
        periods = [startPd + i for i in range(int(endPd - startPd))] 
        lam   = .0001
        gam   = .001
        theta = self.theta

        ctPrices = Series(index=sorted(self.tickers))            
        impactCoefs = self.impactMod
        weights     = self.weights                        
        wealth      = 10000               
   
        for p in periods:
            #tomLog.debug("Begin new period %d" % p)
            pressure    = Series(index=sorted(self.tickers))            
            forecast    = Series(index=sorted(self.tickers))            
            priceUpdate = Series(index=sorted(self.tickers))            
     
            # Step 0: Get the prices for the new period
            #tomLog.debug("Fetch market prices at period: %d" % p )   
            [_getPrices(priceUpdate, t, self.market[t],p) for t in self.tickers]                       
            priceUpdate = priceUpdate.fillna(ctPrices)
            #tomLog.debug("Price Update")         
            #tomLog.debug(priceUpdate.values)
            
            # First pass to initialize prices. Otherwise an entry in ctPrices should
            # never be nan
            if sum(isnull(ctPrices)) > 0:
                ctPrices = priceUpdate 

            # If we haven't seen a tick for all stocks then wait until we do
            if sum(isnull(priceUpdate)) > 0:
                #tomLog.debug("Not all stocks have ticked. Skipping period %d:" % p)
                #tomLog.debug(ctPrices.values)
                continue
            
            # Step 1: Calc PNL
            prcRatio    = priceUpdate.div(ctPrices)            
            logPrcRatio = prcRatio.apply(lambda r: math.log(r))            
            wLogRt      = logPrcRatio.dot(weights)
            wealth      = wealth*math.exp(wLogRt)           
            ctPrices    = priceUpdate  

            #tomLog.debug("Price ratios:")
            #tomLog.debug(prcRatio.values)
            #tomLog.debug("Log Price ratios:")
            #tomLog.debug(logPrcRatio.values)
            #tomLog.debug("Weighted Rt: %0.7f" % wLogRt)
            #tomLog.debug("Compounded wealth: %0.7f" % wealth)

            # Step 2: Compute alphas (& risk if we are doing online covariance)
            #tomLog.debug("Fetch book pressure at period: %d" % p )            
            [_getBookPressure(pressure, t, self.market[t],p)      for t in self.tickers]    
            pressure = pressure.fillna(0)
            #tomLog.debug(pressure.values)            
            #tomLog.debug("Fetch forecast at period: %d" % p )                        
            [_getForecast(forecast, pressure, t, self.tobModels)  for t in self.tickers]
            forecast = forecast.fillna(0)            
            #tomLog.debug(forecast.values)

            # Step 3: Run cost aware optimization
            trades = np.array([0.001, 0.001, 0.001]) # This should start with the current positions            
            c = (weights, forecast, ctPrices, self.impactMod, self.covMat, lam, gam, wealth)            
            weightbounds = [(-0.1,0.1) for i in range(len(self.tickers))]
            res = fmin_slsqp(objFunction, trades, eqcons=[_eqCons], bounds=weightbounds, args=c, acc=1e-7, full_output=True)               
            
            #tomLog.debug("trades:")
            #tomLog.debug(res[0])
            #tomLog.debug("fx:")
            #tomLog.debug(res[1])
            #tomLog.debug("its:")
            #tomLog.debug(res[2])

            # Step 4: Compute deltas from current portfolio and apply thetas
            dWeights    = np.multiply(res[0], theta)
            #tomLog.info("DWeights:")
            #tomLog.info(dWeights)
            
            #usdTradeAmt = sum(dWeights * wealth) 
            #tomLog.info("USD Trade Amt: %0.15f" % usdTradeAmt)
            
            # Step 5: Charge impact cost & update weights
            shrs = weightsToShares(wealth, dWeights, ctPrices)             
            #tomLog.info("Shares:")
            #tomLog.info(shrs.values)
            
            costbp   = _getImpactCost(shrs, self.impactMod)
            usdCosts = np.multiply(costbp, abs(np.multiply(wealth, dWeights))) # Cost in bps on notional of trade               
            wealth   = wealth - sum(usdCosts)
            weights += dWeights   

            #tomLog.debug("Cost(bps): %0.15f" % costbp)
            #tomLog.debug("Cost($):")
            #tomLog.debug(usdCosts)
            #tomLog.debug("Wealth($): %0.7f" % wealth)
            #tomLog.debug("New Weights:")
            #tomLog.debug(weights.values)
        return wealth

def backtestDay(sqlc, d, t): 
    w= RunBacktestDay(d, CacheConfig.prqCache, sqlc, BacktestConfig.tickers, t).run()
    out = {'day':d, 'theta':t, 'wealth':w}    
    pickle.dump(out,open('output/Backtest_%0.3f_%s' % (t,d), 'wb'))    

if __name__ == '__main__':
    sc    = SparkContext(appName="BacktestApp")
    sqlc  = SQLContext(sc)   
     
    logger_name = 'tomLog'
    log_file = 'tomLog.log'
    l = logging.getLogger(logger_name)
    formatter = logging.Formatter('%(asctime)s : %(message)s')
    fileHandler = logging.FileHandler(log_file, mode='wb')
    fileHandler.setFormatter(formatter)    
    l.setLevel("DEBUG")
    l.addHandler(fileHandler)
    tomLog = logging.getLogger('tomLog')
    tomLog.info("Starting")
