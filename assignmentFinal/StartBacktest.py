from multiprocessing import Process
from jobs.RunBacktestDay import backtestDay
from pyspark        import SparkContext
from pyspark.sql    import SQLContext
from BacktestConfig import BacktestConfig

def main():
    sc    = SparkContext(appName='BacktestApp')
    sqlc  = SQLContext(sc)     
  
    threadList = []
    [threadList.append(Process(target=backtestDay, args=(sqlc, d, t))) for d in BacktestConfig.days for t in BacktestConfig.thetas]
    [thread_.run() for thread_ in threadList]
   
if __name__ == '__main__':
    main()
