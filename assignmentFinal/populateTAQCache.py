import os
import scipy.io
from optparse import OptionParser
from pandas import DataFrame, Series
from datetime import datetime, timedelta
import pickle

def _parseArgs():
    parser = OptionParser()  
    parser.add_option("-b", "--begin_date",     dest="begin_date",   default='20070620')
    parser.add_option("-e", "--end_date",       dest ='end_date',    default ='20070625')
    parser.add_option("-v", "--verbose",        action='store_true', dest='verbose')
    parser.add_option("-p", "--populate_cache", action='store_true', dest='populate_cache')
    parser.add_option("-c", "--cacheDir",       dest='cache_dir',    default='C:/Users/tjw5061/TAQ/json')
    parser.add_option("-f", "--tickerFilter",   dest='tick_filter',  default='filters/djiaTickers.txt')
    return parser.parse_args()

def main():
    (opts, args) = _parseArgs()    
   
    # Calculate the tickerList
    with open(opts.tick_filter, 'rb') as f:
        rawTickerList = f.readlines()   
        tickerList = [elem.rstrip('\n') for elem in rawTickerList]
    
        for currentTicker in tickerList:
            startDate   = datetime.strptime(opts.begin_date, '%Y%m%d')
            endDate     = datetime.strptime(opts.end_date, '%Y%m%d')
            currentDate = startDate

            while currentDate != endDate:
                # Import .mat and extract relevant columns into lists
                quoteFileString = 'C:/Users/tjw5061/TAQ/quotes/%s/%s_quotes.mat' % (datetime.strftime(currentDate, '%Y%m%d'), currentTicker)
                tradeFileString = 'C:/Users/tjw5061/TAQ/trades/%s/%s_trades.mat' % (datetime.strftime(currentDate, '%Y%m%d'), currentTicker)
                try:
                    quoteData = scipy.io.loadmat(quoteFileString)
                    tradeData = scipy.io.loadmat(tradeFileString)
                except IOError:
                    currentDate = currentDate + timedelta(days=1)
                    continue

                # Massage .mat lists into quoteFrame
                quoteEpochTime    = [elem[0] for elem in quoteData['data'][0][0][0]]
                # quoteNumRecords = [elem[0] for elem in quoteData['data'][0][0][1]]
                quoteMilliFromMid = [elem[0] for elem in quoteData['data'][0][0][2]]
                quoteBidSize      = [elem[0] for elem in quoteData['data'][0][0][3]]
                quoteBidPrice     = [elem[0] for elem in quoteData['data'][0][0][4]]
                quoteAskSize      = [elem[0] for elem in quoteData['data'][0][0][5]]
                quoteAskPrice     = [elem[0] for elem in quoteData['data'][0][0][6]]

                currentQuoteFrame = DataFrame(index=quoteMilliFromMid)
                currentQuoteFrame['quoteBidSize']   = Series(quoteBidSize,  index=quoteMilliFromMid)
                currentQuoteFrame['quoteBidPrice']  = Series(quoteBidPrice, index=quoteMilliFromMid)
                currentQuoteFrame['quoteAskSize']   = Series(quoteAskSize,  index=quoteMilliFromMid)
                currentQuoteFrame['quoteAskPrice']  = Series(quoteAskPrice, index=quoteMilliFromMid)
                currentQuoteFrame['secondsFromMid'] = Series(map(lambda x: x / 1000, quoteMilliFromMid), index=quoteMilliFromMid)
                currentQuoteFrame['timestamp'] = currentQuoteFrame.index/1000 + quoteEpochTime
                currentQuoteFrame['datetime']  = currentQuoteFrame['timestamp'].apply(lambda x: datetime.fromtimestamp(x))
                currentQuoteFrame['midPrice']  = (currentQuoteFrame['quoteBidPrice'] + currentQuoteFrame['quoteAskPrice'])/2

                # Massage .mat lists into tradeFrame
                tradeEpochTime    = [elem[0] for elem in tradeData['data'][0][0][0]]
                # tradeNumRecords = [elem[0] for elem in tradeData['data'][0][0][1]]
                tradeMilliFromMid = [elem[0] for elem in tradeData['data'][0][0][2]]
                tradeSize         = [elem[0] for elem in tradeData['data'][0][0][3]]
                tradePrice        = [elem[0] for elem in tradeData['data'][0][0][4]]

                currentTradeFrame = DataFrame(index=tradeMilliFromMid)
                currentTradeFrame['tradeSize']  = Series(tradeSize, index=tradeMilliFromMid)
                currentTradeFrame['tradePrice'] = Series(tradePrice, index=tradeMilliFromMid)
                currentTradeFrame['timestamp']  = currentTradeFrame.index/1000 + tradeEpochTime
                currentTradeFrame['datetime']   = currentTradeFrame['timestamp'].apply(lambda x: datetime.fromtimestamp(x))
                currentTradeFrame['secondsFromMid'] = Series(map(lambda x: x / 1000, tradeMilliFromMid), index=tradeMilliFromMid)
                currentTradeFrame = currentTradeFrame.set_index('datetime')

                s1 = currentQuoteFrame.to_json(orient='records')
                s2 = currentTradeFrame.to_json(orient='records')
                pickle.dump(s1, open('%s/quotes/%s_%s_quotes.pkl' % (opts.cache_dir, currentTicker, datetime.strftime(currentDate, '%Y%m%d')), 'wb'))
                pickle.dump(s2, open('%s/trades/%s_%s_trades.pkl' % (opts.cache_dir, currentTicker, datetime.strftime(currentDate, '%Y%m%d')), 'wb'))
                currentDate = currentDate + timedelta(days=1)

if __name__ == "__main__":
    main()
