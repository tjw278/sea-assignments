import numpy as np
from datetime import datetime, timedelta

class BacktestConfig:
    tickers = ['AAPL','CAT','DD']    
    thetas = [0.01, 0.05]
    days   = [datetime.strftime(datetime(2007,06,20),'%Y%m%d')] 
    #thetas  = np.linspace(0.0,0.1,20)     
    #stDate  = datetime(2007,06,20)
    #numDays = 2 
    #days    = [stDate + timedelta(days=x) for x in range(0, numDays)]

class TOBConfig:
    tickers = ['DD','CAT','AAPL']
    stDate  = datetime(2007,06,20)
    numDays = 2 
    days    = [stDate + timedelta(days=x) for x in range(0, numDays)]

class CacheConfig:  
    baseCache = 'data'    
    prqCache  = 'data/parquet'
    pklCache  = 'data/pkl'    

class AggTradesConfig:
    tickers = ['AAPL','CAT','DD']

class AggQuotesConfig:
    tickers = ['AAPL','CAT','DD']

class ImpModelConfig: 
     tickers = ['AAPL','CAT','DD']

class CorrelConfig: 
     tickers = ['AAPL','CAT','DD']


if __name__ == '__main__':
    print BacktestConfig.prqCache
