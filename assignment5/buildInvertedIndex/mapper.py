#!/usr/bin/env python
import sys
import json
from nltk.tokenize import RegexpTokenizer

tokenizer = RegexpTokenizer(r'\w+')
for line in sys.stdin:
    _id    = json.loads(line)['id']
    _body  = json.loads(line)['body']
    _title = json.loads(line)['title']

    tokenizedBody = tokenizer.tokenize(_body.lower())
    for term in set(tokenizedBody):
        print (_id, (term, tokenizedBody.count(term)))
