#!/usr/bin/env python

import sys
import json

invertedIndex = {}
for line in sys.stdin:
    (_id, termFreqTup) = eval(line.strip('\n'))
    if termFreqTup[0] in invertedIndex:
        invertedIndex[termFreqTup[0]].append((_id, termFreqTup[1]))
    else:
        invertedIndex[termFreqTup[0]] = [(_id, termFreqTup[1])]

print json.dumps(invertedIndex)
