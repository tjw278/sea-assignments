#!/usr/bin/env python
import os
import pickle
import json
import subprocess
from assignment4.coordinator import Job
from assignment4.inventory import Inventory

def _joinIDFDicts(x,y):
    z = {}
    (inner,outer) = (x,y) if len(x) > len(y) else (y,x)
    for k,v in outer.iteritems():
        if k in inner:
            z[k] = inner[k] + v
        else:
            z[k] = v
    return z


def main():
    inv = Inventory()
    jobPath = 'assignment5/indexjobs'

    buildInvertedIndexMapper  = 'assignment5/buildInvertedIndex/mapper.py'
    buildInvertedIndexReducer = 'assignment5/buildInvertedIndex/reducer.py'
    Job(jobPath, buildInvertedIndexMapper, buildInvertedIndexReducer, inv).run()
    [pickle.dump(json.load(open('%d.out' % i, 'rb')), open('InvertedIndex_%d.pkl' % i, 'w')) for i in range(inv.numServers)]
    [os.remove('%d.out' % i) for i in range(inv.numServers)]

    buildDocStoreMapper  = 'assignment5/buildDocStore/mapper.py'
    buildDocStoreReducer = 'assignment5/buildDocStore/reducer.py'
    Job(jobPath, buildDocStoreMapper, buildDocStoreReducer, Inventory()).run()
    [pickle.dump(json.load(open('%d.out' % i, 'rb')), open('DocStore_%d.pkl' % i, 'w')) for i in range(inv.numServers)]
    [os.remove('%d.out' % i) for i in range(inv.numServers)]

    buildDocStoreMapper  = 'assignment5/buildIDFMap/mapper.py'
    buildDocStoreReducer = 'assignment5/buildIDFMap/reducer.py'
    Job(jobPath, buildDocStoreMapper, buildDocStoreReducer, Inventory()).run()
    counts = [json.load(open('%d.out' % i,'rb')) for i in range(inv.numServers)]
    out = {}
    for c in counts:
        out = _joinIDFDicts(out,c)
    pickle.dump(c,open('DocFreq.pkl','w'))
    [os.remove('%d.out' % i) for i in range(inv.numServers)]

if __name__ == '__main__':
    main()
