#!/usr/bin/env python
import sys
import json

for line in sys.stdin:
    _id    = json.loads(line)['id']
    _body  = json.loads(line)['body']
    _title = json.loads(line)['title']

    print((_id, (_title, _body)))
