#!/usr/bin/env python
import sys
import json

docStore = {}
for line in sys.stdin:
    (_id, titleBodyTup) = eval(line.strip('\n'))
    docStore[_id] = (titleBodyTup[0], titleBodyTup[1])

print json.dumps(docStore)
