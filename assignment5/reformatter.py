#!/usr/bin/env python
from optparse import OptionParser
import xml.etree.ElementTree
import json

class Reformatter:
    def __init__(self, corpus, path, n):
        self.corpus = corpus
        self.path = path
        self.numIdxPartitions = n

    def run(self):
        # Open our .in partitions for writing
        inFileItr = 0
        inputFiles = [open('indexjobs/%s.in' % i, 'w') for i in range(int(self.numIdxPartitions))]

        # Read in the DOM
        tree = xml.etree.ElementTree.parse(self.corpus)
        root = tree.getroot()

        # For each page format a json string and write it to our files round robin style
        prefix = 'http://www.mediawiki.org/xml/export-0.10/'
        for page in root.findall('{%s}page' % prefix):
            d = {'title': page.find('{%s}title' % prefix).text,
                 'id'   : page.find('{%s}id' % prefix).text,
                 'body' : page.find('{%s}revision' % prefix).find('{%s}text' % prefix).text}
            line = json.dumps(d)
            a = json.loads(line)
            print a['id']

            inputFiles[inFileItr % int(self.numIdxPartitions)].write(line + '\n')
            inFileItr += 1

if __name__ == '__main__':
    o = OptionParser()
    o.add_option('--jobPath', dest='jobPath')
    o.add_option('--numPartitions', dest='numPartitions')
    opts, args = o.parse_args()

    Reformatter(corpus=args[0], path=opts.jobPath, n=opts.numPartitions).run()