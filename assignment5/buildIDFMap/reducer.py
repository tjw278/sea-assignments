#!/usr/bin/env python
import sys
import json

termFreqDict = {}
for line in sys.stdin:
    (_id, termFreqTup) = eval(line.strip('\n'))
    term = termFreqTup[0]
    freq = termFreqTup[1]

    if term in termFreqDict:
        termFreqDict[term] = termFreqDict[term] + int(freq)
    else:
        termFreqDict[term] = int(freq)

print json.dumps(termFreqDict)