import socket
import tornado.ioloop
import tornado.web

class FrontendHandler(tornado.web.RequestHandler):          
    def initialize(self, port):
        self.port = port
    
    def get(self):
        self.write('%s:%s' % (socket.gethostname(), str(self.port)) )
       
def goRequestHandler(base_port):
    # Constuct the web app
    application = tornado.web.Application([
        (r"/", FrontendHandler, dict(port=base_port) )
    ])

    ## Fire it up ...
    print "Listening on port: %d " % base_port
    application.listen(base_port)
    tornado.ioloop.IOLoop.instance().start()

if __name__ == "__main__":
    goRequestHandler(8080)