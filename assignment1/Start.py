from multiprocessing import Process
from SimpleRequestHandler import goRequestHandler
from SimpleLoadBalancer import goLoadBalancer


def main():
    loadBalancer     = Process(target = goLoadBalancer,   args=(10000,))
    requestHandlers1 = Process(target = goRequestHandler, args=(10001,))
    requestHandlers2 = Process(target = goRequestHandler, args=(10002,))
    requestHandlers3 = Process(target = goRequestHandler, args=(10003,))
    
    loadBalancer.start()
    requestHandlers1.start()
    requestHandlers2.start()
    requestHandlers3.start()

if __name__ == '__main__':
    main()