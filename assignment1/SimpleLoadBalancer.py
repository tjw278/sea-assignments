import json
import tornado.ioloop
from tornado.web import Application 
from tornado.httpclient import AsyncHTTPClient
from tornado import gen

GLOBALCOUNT = 0 

class FrontendHandler(tornado.web.RequestHandler):    
    def initialize(self, backendList):
        self.backendList = backendList    
    
    @gen.coroutine
    def get(self):
        global GLOBALCOUNT             
        http_client = AsyncHTTPClient()
        response = yield http_client.fetch('http://%s:8080' % str(self.backendList[(GLOBALCOUNT % 3)]))
        self.write(response.body)            
        GLOBALCOUNT = GLOBALCOUNT + 1     
      
def goLoadBalancer(port, backends):
    # Constuct the web app
    application = Application([
        (r"/", FrontendHandler, dict(backendList=backends))
    ])

    ## Fire it up ...
    print "Listening on port: %d " % port
    application.listen(port)
    tornado.ioloop.IOLoop.instance().start()

if __name__ == "__main__":
    json_data = open('server.cache')   
    data = json.load(json_data)     
    goLoadBalancer(80, data['backends'])
    
