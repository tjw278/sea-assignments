import math
import pickle
import tornado.ioloop
import tornado.web
import pandas as pd
import numpy as np
from nltk.tokenize import RegexpTokenizer
from tornado import gen

class IndexHandler(tornado.web.RequestHandler):
    def initialize(self, termDocListMap, termDocFreqMap):
        self.termDocListMap = termDocListMap
        self.termDocFreqMap = termDocFreqMap
        self.tokenizer = RegexpTokenizer(r'\w+')

    def tdfIdf(self, tf, n, N):
        '''
        :param tf: number of occurrences of this term in a document
        :param n:  number of documents that contain this term
        :param N:  number of documents in the entire collection

        Term frequency with a scalar multiplier that increases with the
        rareness of the term in the corpus
        '''
        return float(tf) * math.log(float(N)/float(n), 2)

    @gen.coroutine
    def get(self, queryString):
        tokenizedrequest = self.tokenizer.tokenize(queryString)
        vectorSpace = pd.DataFrame(index=tokenizedrequest)
        vectorSpace['webrequest'] = np.array([self.tdfIdf(1, self.termDocFreqMap[tok], len(self.termDocFreqMap)) for tok in tokenizedrequest])

        for tok in tokenizedrequest:
            numDocsInCollection = len(self.termDocFreqMap)
            numDocsWithTerm = self.termDocFreqMap['%s' % tok]

            if tok in self.termDocListMap:  # Might not be on all index servers
                for termCountTuple in self.termDocListMap[tok]:
                    docId = termCountTuple[0]
                    if docId in vectorSpace.columns:
                        vectorSpace.loc[tok, docId] = self.tdfIdf(termCountTuple[1], numDocsWithTerm, numDocsInCollection)
                    else:
                        vectorSpace[docId] = np.zeros(len(tokenizedrequest))
                        vectorSpace.loc[tok, docId] = self.tdfIdf(termCountTuple[1], numDocsWithTerm, numDocsInCollection)

        results = vectorSpace.apply(lambda x: np.inner(x.values, vectorSpace['webrequest'].values))
        results = results.drop('webrequest')
        results.sort(ascending=False)
        results = results.head(10)
        self.write({'postings': zip(results.index, results.values)})

def startIndexServer(port_, partition):
    # Take a sample term
    invertedindex = pickle.load(open("InvertedIndex_%d.pkl" % partition, "rb"))
    docfreq = pickle.load(open("DocFreq.pkl", "rb"))

    # Construct the web app
    application = tornado.web.Application([
        (r"/index/(.*)", IndexHandler, dict(termDocListMap=invertedindex, termDocFreqMap=docfreq))
    ])

    # Fire it up ...
    print "Index server partition: %d listening on port: %d " % (partition, port_)
    application.listen(port_)
    tornado.ioloop.IOLoop.instance().start()

if __name__ == "__main__":
    startIndexServer(8080, 1)
