import pickle
import tornado.ioloop
import tornado.web
from tornado import gen


class DocRequestHandler(tornado.web.RequestHandler):
    def initialize(self, docData):
        self.docData = docData

    @gen.coroutine
    def get(self, queryString):
        # Queries should be of the form {docid}_{query terms}
        querystringsplit = queryString.split('_')
        docid = querystringsplit[0]
        searchterm = querystringsplit[1]

        loc = self.docData[docid][1].lower().find(searchterm.lower())
        docLength = len(self.docData[docid][1])
        offset = min((loc-1), (docLength - loc - 1))
        if offset > 200:
            offset = 200

        queryresult = dict(
            snippet=('...' + self.docData[docid][1][(loc - offset):(loc + offset)] + '...').replace(searchterm, "<strong>" + searchterm + "</strong>"),
            title=self.docData[docid][0],
            url='http://en.wikipedia.org/wiki/%s' % self.docData[docid][0].replace(' ', '_'))
        self.write('%s' % queryresult)

def startDocumentServer(port_, partition):

    docStoreData = pickle.load(open('DocStore_%d.pkl' % partition, 'rb'))

    # Construct the web app
    application = tornado.web.Application([
        (r"/doc/(.*)", DocRequestHandler, dict(docData=docStoreData))
    ])

    #Fire it up ...
    print "Document server partition %d listening on port: %d " % (partition, port_)
    application.listen(port_)
    tornado.ioloop.IOLoop.instance().start()

if __name__ == "__main__":
    startDocumentServer(8081, 1)
