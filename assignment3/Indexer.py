import pickle
import xml.etree.ElementTree
from nltk.tokenize import RegexpTokenizer

def termFreqInsert(termTriple, termFreqDict, partition):
    if not termTriple[0] in termFreqDict[partition]:
        termFreqDict[partition][termTriple[0]] = [(termTriple[1], termTriple[2])]
    else:
        termFreqDict[partition][termTriple[0]].append((termTriple[1], termTriple[2]))

def docFreqInsert(term, docFreqMap):
    if not term in docFreqMap:
        docFreqMap[term] = 1
    else:
        docFreqMap[term] += 1

def dictOfDictsInsert(d, idx):
    d[idx] = {}

#def docPrint(k,d):
#    print "Length of doc share %d : %d" % (k, len(d))

#def termPrint(k,d):
#    print "Length of term freq %d : %d" % (k, len(d))

# Machine inventory
DocServerCount = 3
IdxServerCount = 3
termFreqDicts = {}
docShareDicts = {}

# All the tags in this DOM are prefixed with this string
[dictOfDictsInsert(termFreqDicts, n) for n in range(IdxServerCount)]
[dictOfDictsInsert(docShareDicts, n) for n in range(DocServerCount)]
map_docId_docFreq = {}

# Read in the DOM
tree = xml.etree.ElementTree.parse('info_ret.xml')
root = tree.getroot()

prefix = 'http://www.mediawiki.org/xml/export-0.10/'
for page in root.findall('{%s}page' % prefix):
    
    # Prepare for document store shard
    title = page.find('{%s}title' % prefix).text
    id_ = page.find('{%s}id' % prefix).text
    body = page.find('{%s}revision' % prefix).find('{%s}text' % prefix).text

    docPartition = int(id_) % DocServerCount
    idxPartition = int(id_) % IdxServerCount

    # Insert into correct document store shard
    docShareDicts[docPartition][id_] = (title, body)

    # Prepare for term inverted index shard
    body = body.lower()
    tokenizer = RegexpTokenizer(r'\w+')
    tokenizedBody = tokenizer.tokenize(body)

    # Insert into correct term inverted index shard
    termCounts = [(term, id_, tokenizedBody.count(term)) for term in set(tokenizedBody)]
    [termFreqInsert(termCount, termFreqDicts, idxPartition) for termCount in termCounts]

    # Insert into complete term <-> doc frequency map
    [docFreqInsert(termCount[0], map_docId_docFreq) for termCount in termCounts]

[pickle.dump(d, open('DocStore_%d.pkl' % k, 'wb')) for (k, d) in docShareDicts.items()]
[pickle.dump(d, open('InvertedIndex_%d.pkl' % k, 'wb')) for (k, d) in termFreqDicts.items()]
pickle.dump(map_docId_docFreq, open('DocFreq.pkl', 'wb'))
#[termPrint(k, d) for (k, d) in docShareDicts.items()]
#[docPrint(k, d)for (k, d) in termFreqDicts.items()]
