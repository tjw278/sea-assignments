from multiprocessing import Process
from FrontEndServer import startSearchFrontEnd
from IndexServer    import startIndexServer
from DocumentServer import startDocumentServer

def insertIndexServerThread(threadDict, portSeed, partition):
    threadDict[partition] = Process(target=startIndexServer, args=(portSeed + partition, partition))

def insertDocServerThread(threadDict, portSeed, partition):
    threadDict[partition] = Process(target=startDocumentServer, args=(portSeed + partition, partition))


def main():
    numIdxServers = 3
    numDocServers = 3
    idxServerThreadDict = {}
    docServerThreadDict = {}
    [insertIndexServerThread(idxServerThreadDict, 8000, n) for n in range(numIdxServers)]
    [insertDocServerThread(docServerThreadDict, (8000 + numIdxServers), n) for n in range(numDocServers)]
    frontEnd = Process(target=startSearchFrontEnd, args=(8080, numIdxServers, numDocServers, 8000, ))

    [thread_.start() for (partition_, thread_) in idxServerThreadDict.items()]
    [thread_.start() for (partition_, thread_) in docServerThreadDict.items()]
    frontEnd.start()

if __name__ == '__main__':
    main()