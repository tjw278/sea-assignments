from pandas import DataFrame
import tornado.ioloop
from tornado.web import Application
from tornado.httpclient import AsyncHTTPClient
from tornado import gen

# Todo -- title gets more points
# Todo -- sharding

class FrontendHandler(tornado.web.RequestHandler):
    def initialize(self, idxServerCount, docServerCount, portSeed):
        self.numIdxServers = int(idxServerCount)
        self.numDocServers = int(docServerCount)
        self.portSeed      = int(portSeed)

    @gen.coroutine
    def get(self, queryString):
        http_client = AsyncHTTPClient()
        completeResponse = {}
        for n in range(self.numIdxServers):
            currentResponse = yield http_client.fetch('http://localhost:%d/index/%s' % (self.portSeed + n, queryString))
            completeResponse.update(eval(currentResponse.body)['postings'])

        mergedResponse = DataFrame.from_dict(completeResponse, orient='index')
        mergedResponse = mergedResponse.sort(0, ascending=False)
        mergedResponse = mergedResponse.head(10)
        print 'Merged Response Head:'
        print mergedResponse.head(1)

        for docId, row in mergedResponse.iterrows():
            currentPortPartition = self.portSeed + self.numIdxServers + (hash(docId) % int(self.numDocServers))
            print 'Expected partition: %d Port: %d' % (currentPortPartition, (hash(docId) % int(self.numDocServers)))
            docServeResponse = yield http_client.fetch('http://localhost:%d/doc/%s_%s' % (currentPortPartition, docId, queryString))
            self.write('<br> %s <br>' % docServeResponse.body)

def startSearchFrontEnd(port, numIndexServers, numDocServers, backendPortSeed):
    # Constuct the web app
    application = Application([
        (r"/search/(.*)", FrontendHandler, dict(idxServerCount=numIndexServers, docServerCount=numDocServers, portSeed=backendPortSeed)),
    ])

    ## Fire it up ...
    print "Listening on port: %d " % port
    application.listen(port)
    tornado.ioloop.IOLoop.instance().start()

if __name__ == "__main__":
    startSearchFrontEnd(8080, 3, 3, 8000)
