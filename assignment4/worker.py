#!/usr/bin/env python
import os
import json
import tornado.ioloop
import tornado.web
import subprocess
from random import randrange
from tornado import gen
from tornado.httpclient import AsyncHTTPClient
from inventory import Inventory
from multiprocessing import Process

def _createPartitions(dict_, partIdx):
    dict_[partIdx] = []
    
class mapHandler(tornado.web.RequestHandler):
    buff = {}

    def head(self):
       self.finish()

    @gen.coroutine
    def get(self):
        mapperPath  = self.get_query_argument('mapperPath')
        inputFile   = self.get_query_argument('inputFile')
        numReducers = self.get_query_argument('numReducers')
        mapTaskID   = randrange(0, 100000000)
        
        # Run the mapper
        d = {}
        [_createPartitions(d, i) for i in range(int(numReducers))]
        catProcess = subprocess.Popen(["cat", inputFile], stdout=subprocess.PIPE)
        mapProcess = subprocess.Popen(["python", mapperPath], stdin=catProcess.stdout, stdout=subprocess.PIPE)
        catProcess.stdout.close()
        output = mapProcess.communicate()[0]

        # Buffer the response
        for elem in output.split('\n'):
                try:
                    tup = eval(elem)
                    _part = hash(tup[0]) % int(numReducers) # If our key is non-numerical we have to hash
                    _key = tup[0]
                    _val = tup[1]
                    d[_part].append((_key,_val))
                except SyntaxError:
                    pass
        mapHandler.buff[mapTaskID] = d

        # Report back your successes
        response = dict(status='success', mapTaskID=mapTaskID)
        self.write(response)

class retrieveMapOutputHandler(tornado.web.RequestHandler):
    @gen.coroutine
    def get(self):
        reducerIx  = self.get_query_argument('reducerIx')
        mapTaskID  = self.get_query_argument('mapTaskID')
        kvpTuples  = mapHandler.buff[int(mapTaskID)][int(reducerIx)]
        output = {'output': kvpTuples }
        self.write(output)
  
class reduceHandler(tornado.web.RequestHandler):
    def initialize(self, port):
        self.port = port

    @gen.coroutine
    def get(self):
        reducerIx  =self.get_query_argument('reducerIx')
        reducerPath=self.get_query_argument('reducerPath')
        mapTaskIDs =self.get_query_argument('mapTaskIDs')
        jobPath    =self.get_query_argument('jobPath')
        mapTaskIDs = mapTaskIDs.split(',')  

        responses = []
        for elem in mapTaskIDs:
            httpclient = AsyncHTTPClient()
            fut = httpclient.fetch('http://localhost:%s/retrieveMapOutput?reducerIx=%s&mapTaskID=%s' % (i.next(),reducerIx,elem))
            resp = yield fut
            responses.append(eval(resp.body)['output'])

        reduceProcess = subprocess.Popen(["python", reducerPath], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        strKvpList = [str(kvp) for mapTsk in responses for kvp in mapTsk]
        temp = '\n'.join(strKvpList)
        reduceProcess.stdin.write(temp)
        output = reduceProcess.communicate()[0]
        with open('%s.out' % str(reducerIx), 'w') as f:
            f.write(output)
            f.close()
        
class Worker:
    buff = {}
    def __init__(self, port_):
        self.port = port_

    def startWorker(self):
        # Construct the web app
        application = tornado.web.Application([
            (r"/map",               mapHandler),
            (r"/retrieveMapOutput", retrieveMapOutputHandler),
            (r"/reduce",            reduceHandler, dict(port=self.port))
        ])

        # Fire it up ...
        print "Index server listening on port: %d " % self.port
        application.listen(self.port)
        tornado.ioloop.IOLoop.instance().start()

def workerFactory(port_):
    print 'Listening on port %0.0f' % int(port_)
    w = Worker(int(port_))
    w.startWorker()

if __name__ == "__main__":    
    # Spawn Servers
    i = Inventory()
    threadlist = [Process(target=workerFactory, args=(p,)) for p in i.ports]
    [t.start() for t in threadlist]

