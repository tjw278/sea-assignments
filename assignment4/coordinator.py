import os
from optparse import OptionParser
from inventory import Inventory
from multiprocessing import Process, Queue
from worker import Worker
from tornado import gen
from tornado.ioloop import IOLoop
from tornado.httpclient import AsyncHTTPClient


def fetch_map(port, mapperPath, inputFile, numReducers, client):
    return client.fetch('http://localhost:%0.0f/map?mapperPath=%s&inputFile=%s&numReducers=%0.0f' % (port,mapperPath,inputFile,numReducers))

def fetch_reduce(port, reducerIx, reducerPath, mapTaskIDs, jobPath, client):
    _str = 'http://localhost:%0.0f/reduce?reducerIx=%s&reducerPath=%s&mapTaskIDs=%s&jobPath=%s' % (port, reducerIx, reducerPath, mapTaskIDs, jobPath)
    return client.fetch(_str)


# This entry point patter I took from Matt's example
class Job:
    def __init__(self, jobPath, mapperPath, reducerPath, inventory):
        self.i = inventory
        self.numReducers = inventory.numServers
        self.mapperPath = mapperPath
        self.jobPath = jobPath
        self.reducerPath = reducerPath

    def run(self):
        IOLoop.current().run_sync(self.runCoroutine)

    @gen.coroutine
    def runCoroutine(self):
        i           = self.i
        numReducers = self.numReducers
        mapperPath  = self.mapperPath
        jobPath     = self.jobPath
        reducerPath = self.reducerPath

        # Map
        mapTaskInputs = [f for f in os.listdir(jobPath) if f.endswith('.in')]
        http = AsyncHTTPClient()
        mapFutures = [fetch_map(i.next(), mapperPath, (jobPath + '/' + m), numReducers, http) for m in mapTaskInputs]
        mapResults = yield mapFutures

        # Reduce
        mapTaskIDs    = [str(eval(m.body)['mapTaskID']) for m in mapResults]
        reduceFutures = [fetch_reduce(i.next(), elem, reducerPath, ','.join(mapTaskIDs), jobPath, http) for elem in range(numReducers)]
        reduceResults = yield reduceFutures

def main():
    o = OptionParser()
    o.add_option('-j', '--jobPath'    , dest='jobPath',     default='fish_jobs')
    o.add_option('-m', '--mapperPath' , dest='mapperPath',  default='mapper.py')
    o.add_option('-r', '--reducerPath', dest='reducerPath', default='reducer.py')
    (opts, args) = o.parse_args()

    Job(opts.jobPath, opts.mapperPath, opts.reducerPath, Inventory()).run()

if __name__ == '__main__':
    main()
