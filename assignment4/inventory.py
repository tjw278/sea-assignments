
class Inventory:
    def __init__(self):
        self.numServers = 3
        self.seedPort = 5000
        self.ports = ['%d' % (self.seedPort + i) for i in range(self.numServers)]
        self.index = 0

    def next(self):
        portToReturn = self.seedPort + (self.index % self.numServers)
        self.index = self.index + 1
        return portToReturn
